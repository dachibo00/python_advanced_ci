from fastapi.testclient import TestClient

from src.main import app

client = TestClient(app)

test_recipe = {"name": "test", "cooking_time": 120, "description": "test"}
test_ingredient = {"name": "test"}


def test_add_recipe():
    response = client.post("/add_recipe/", json=test_recipe)
    test_recipe.update({"id": response.json()["id"]})
    test_ingredient.update({"recipe_id": response.json()["id"]})
    assert response.status_code == 200
    assert response.json() == test_recipe


def test_add_ingredient():
    response = client.post("/add_ingredient/", json=test_ingredient)
    test_ingredient.update({"id": response.json()["id"]})
    assert response.status_code == 200
    assert response.json() == test_ingredient


def test_get_recipe_id():
    response = client.get(f"/all_recipes_for_id/{test_recipe['id']}")
    test_recipe.update({"ingredient": [{"name": test_recipe["name"]}]})
    assert response.status_code == 200
    assert response.json() == test_recipe


def test_get_all_recipes():
    response = client.get("/all_recipes/")
    assert response.status_code == 200
