from typing import List

import uvicorn
from fastapi import FastAPI
from sqlalchemy import select, update
from sqlalchemy.orm import selectinload

import models as models
import schemas as schemas
from database import engine, session

app = FastAPI()


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.post("/add_recipe/", response_model=schemas.RecipeOutCreate)
async def add_recipe(recipe: schemas.RecipeCreate):
    new_recipe = models.Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.post("/add_ingredient/", response_model=schemas.IngredientOut)
async def add_recipe(ingredient: schemas.IngredientCreate):
    new_ingredient = models.Ingredient(**ingredient.dict())
    async with session.begin():
        session.add(new_ingredient)
    return new_ingredient


@app.get("/all_recipes/", response_model=List[schemas.RecipeInDbMany])
async def all_recipes():
    res = await session.execute(
        select(models.Recipe).order_by(models.Recipe.count_of_views.desc())
    )
    return res.scalars().all()


@app.get("/all_recipes_for_id/{idx}", response_model=schemas.RecipeInDbOnly)
async def all_recipes_for_id(idx: int):
    async with session.begin():
        query = (
            update(models.Recipe)
            .where(models.Recipe.id == idx)
            .values(count_of_views=models.Recipe.count_of_views + 1)
        )
        await session.execute(query)
        statement = (
            select(models.Recipe)
            .options(selectinload(models.Recipe.ingredient))
            .where(models.Recipe.id == idx)
        )
        result = await session.execute(statement)
        recipe = result.scalars().first()
    return recipe


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
