from typing import List, Optional

from pydantic import BaseModel


class BaseRecipe(BaseModel):
    name: str
    cooking_time: int


class RecipeCreate(BaseRecipe):
    description: Optional[str] = None


class RecipeOutCreate(RecipeCreate):
    id: int

    class Config:
        orm_mode = True


class RecipeInDbMany(RecipeOutCreate):
    count_of_views: int

    class Config:
        orm_mode = True


class BaseIngredient(BaseModel):
    name: str

    class Config:
        orm_mode = True


class IngredientCreate(BaseIngredient):
    recipe_id: int


class IngredientOut(IngredientCreate):
    id: int

    class Config:
        orm_mode = True


class RecipeInDbOnly(BaseRecipe):
    id: int
    description: Optional[str] = None
    ingredient: List[BaseIngredient] = []

    class Config:
        orm_mode = True
