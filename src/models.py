from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from database import Base


class Recipe(Base):
    __tablename__ = "Recipe"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    count_of_views = Column(Integer, default=0)
    cooking_time = Column(Integer)
    description = Column(String, nullable=True)
    ingredient = relationship("Ingredient")


class Ingredient(Base):
    __tablename__ = "Ingredient"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    recipe_id = Column(Integer, ForeignKey(Recipe.id))
